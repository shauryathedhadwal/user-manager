FROM node:14.17-alpine

# Sets path inside docker container for commands like COPY 
WORKDIR /usr/src/app

# Will copy package.json & package-lock.json
COPY package*.json ./

# npm ci -> Install from package-lock.json 
# --only=production -> Skip dev dependencies
RUN npm ci --ony=production

# Will copy all files from directory where DockerFile is present on host machine to
# path set by WORKDIR above inside docker container
COPY . .

RUN npm run build

CMD ["npm", "start"]