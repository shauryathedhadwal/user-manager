### Important

-   Phone/mobile numbers should be stored as strings. (phonenumbers starting with 0?)

### Gotchas

-   Do Phone numbers uniquely identify users?

### TODO

-   ~~Hash Passwords~~
-   ~~Add improved logging~~
-   ~~Create custom classes for error handling~~
-   Add swagger doc
-   Add web hooks
-   Password Recovery
-   User Self Service Registration
-   Add [Typescript namespacing](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Namespaces.md)
-   SonarQube
-   Unit Tests
-   Add support for handlers with onError and onResponse
-   Add mobile number validation

### Resources

-   https://michalzalecki.com/using-sequelize-with-typescript/
-   https://stackabuse.com/using-sequelize-js-and-sqlite-in-an-express-js-app/
-   https://sequelize.org/master/manual/model-querying-basics.html#simple-select-queries
-   https://stackoverflow.com/questions/61305578/what-typescript-configuration-produces-output-closest-to-node-js-14-capabilities/61305579#61305579
