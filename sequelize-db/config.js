const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../environments/.env') })

const getSettings = () => ({
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: 'postgres',
    define: {
        freezeTableName: true,
    },
})

module.exports = {
    local: getSettings(),
    development: getSettings(),
    production: getSettings(),
}
