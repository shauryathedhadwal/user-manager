'use strict'
const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../environments/.env') })
const DB_SCHEMA_PREFIX = process.env.DB_SCHEMA_PREFIX

const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
    class user extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    user.init(
        {
            first_name: DataTypes.STRING,
            last_name: DataTypes.STRING,
            mobile: DataTypes.STRING,
            name: DataTypes.STRING,
            email_id: DataTypes.STRING,
            country_code: DataTypes.STRING,
            password: DataTypes.STRING,
            created_at: DataTypes.DATE,
            updated_at: DataTypes.DATE,
            feature_flag: DataTypes.BOOLEAN
        },
        {
            sequelize,
            modelName: 'user',
            schema: DB_SCHEMA_PREFIX,
        },
    )
    return user
}
