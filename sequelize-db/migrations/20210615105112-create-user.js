'use strict'
const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../environments/.env') })
const DB_SCHEMA_PREFIX = process.env.DB_SCHEMA_PREFIX

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createSchema(DB_SCHEMA_PREFIX)
        await queryInterface.createTable(
            'user',
            {
                id: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                },
                firstName: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    field: 'first_name',
                },
                lastName: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    field: 'last_name',
                },
                name: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    field: 'name',
                },
                mobile: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    unique: true,
                    field: 'mobile',
                },
                emailId: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    unique: true,
                    field: 'email_id',
                },
                password: {
                    type: Sequelize.STRING,
                    allowNull: false,
                    field: 'password',
                },
                countryCode: {
                    type: Sequelize.STRING,
                    allowNull: true,
                    defaultValue: '+91',
                    field: 'country_code',
                },
                createdAt: {
                    type: Sequelize.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.NOW,
                    field: 'created_at',
                },
                updatedAt: {
                    type: Sequelize.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.NOW,
                    field: 'updated_at',
                },
            },
            {
                schema: DB_SCHEMA_PREFIX,
            },
        )
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('users', {
            schema: DB_SCHEMA_PREFIX,
        })
    },
}
