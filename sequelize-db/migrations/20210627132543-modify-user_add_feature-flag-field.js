'use strict'
const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../environments/.env') })
const DB_SCHEMA_PREFIX = process.env.DB_SCHEMA_PREFIX

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.addColumn(
            {
                tableName: 'user',
                schema: DB_SCHEMA_PREFIX,
            },
            'feature_flag', // new field name
            {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }
        )
    },

    down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn(
            {
                tableName: 'user',
                schema: DB_SCHEMA_PREFIX,
            },
            'feature_flag' // new field name
        )
    },
}
