import { Sequelize } from 'sequelize'
import { UserFactory } from './user.model'
import { getAppConfig } from '../configs/app.config'
import Logger from '../shared/logger'

const config = getAppConfig()
const sequelize = new Sequelize({
    username: config.DB_USERNAME,
    password: config.DB_PASSWORD,
    host: config.DB_HOST,
    database: config.DB_NAME,
    dialect: 'postgres',
    pool: {
        min: 0,
        max: 5,
        acquire: 30000,
        idle: 10000,
    },
    logging: (msg) => Logger.debug(msg),
    define: {
        freezeTableName: true,
    },
})

const db = {
    sequelize,
    Sequelize,
    User: UserFactory(sequelize),
}

export default db
