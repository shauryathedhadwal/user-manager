'use strict'

import { BuildOptions, Model, Sequelize, DataTypes } from 'sequelize'
import { getAppConfig } from '../configs/app.config'
import { DB_TABLES } from '../shared/constants'
import { DatabaseGeneratedId, EncryptedString } from '../shared/types'
/**
 * interface IUserAttributes {} // fields of a single database row
 * interface UserInstance {}   // a single database row
 * interface UserModel {}      // a table in the database
 */
export interface IUserAttributes {
    id?: DatabaseGeneratedId
    firstName?: string
    lastName?: string
    name?: string
    mobile: string
    password: EncryptedString
    emailId?: string
    countryCode?: number
    createdAt?: Date
    updatedAt?: Date
    featureFlag?: Boolean
}

interface UserModel extends Model<IUserAttributes>, IUserAttributes {}
class User extends Model<UserModel, IUserAttributes> {}
type UserStatic = typeof Model & {
    new (values?: Record<string, string | number>, options?: BuildOptions): UserModel
}

export function UserFactory(sequelize: Sequelize): UserStatic {
    return <UserStatic>sequelize.define(
        DB_TABLES.USER,
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            firstName: {
                type: DataTypes.STRING,
                allowNull: true,
                field: 'first_name',
                validate: {
                    len: [1, 35],
                },
            },
            lastName: {
                type: DataTypes.STRING,
                allowNull: true,
                field: 'last_name',
                validate: {
                    len: [1, 35],
                },
            },
            name: {
                type: DataTypes.STRING,
                allowNull: true,
                field: 'name',
                validate: {
                    len: [1, 71],
                },
            },
            mobile: {
                type: DataTypes.STRING,
                allowNull: false,
                field: 'mobile',
                validate: {
                    len: [1, 35],
                },
            },
            emailId: {
                type: DataTypes.STRING,
                allowNull: true,
                unique: true, //TODO: Should be unique?
                field: 'email_id',
                validate: {
                    len: [1, 70],
                },
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
                field: 'password',
                validate: {
                    len: [1, 70],
                },
            },
            countryCode: {
                type: DataTypes.NUMBER,
                allowNull: true,
                defaultValue: '+91',
                field: 'country_code',
                validate: {
                    len: [1, 5],
                },
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW,
                field: 'created_at',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: DataTypes.NOW,
                field: 'updated_at',
            },
            featureFlag: {
                type: DataTypes.BOOLEAN,
                allowNull: true,
                field: 'feature_flag'
            }
        },
        {
            schema: getAppConfig().DB_SCHEMA_PREFIX,
        },
    )
}
