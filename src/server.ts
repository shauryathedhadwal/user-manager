import { ENV } from './shared/constants'
import { getAppConfig } from './configs/app.config'
if (getAppConfig().NODE_ENV === ENV.PROD) {
    import('newrelic') /**import before other modules */
}
import express from 'express'
import cors from 'cors'
import { default as UserRouter } from './modules/user/user.router'
import Logger from './shared/logger'
import morgan from './shared/middlewares/morgan'
import { initSentry, initSentryErrorHandlers } from './shared/sentry'
import { ApplicationError, initErrorHandlerController } from './shared/error-handlers'

let app: express.Application

const initMiddlewares = () => {
    app.use(cors())
    app.use(express.urlencoded({ extended: false }))
    app.use(express.json())
    app.use(morgan)
}

// TODO: API Versioning
const initRoutes = () => {
    app.use('/api/user', UserRouter)
}

const initMiscellaneousRoutes = () => {
    app.get('/health', (req, res) => {
        res.json({ isAlive: true })
    })
    app.get('/debug-sentry', (req, res) => {
        throw new ApplicationError('Sentry Error!')
    })
}

const startServer = () => {
    getAppConfig()
    app = express()
    initSentry(app) /** Should be called right after app init */
    initMiddlewares()
    initRoutes()
    initMiscellaneousRoutes()
    initSentryErrorHandlers(app) /** Should be called before other Error Handlers */
    initErrorHandlerController(app)

    const port = process.env.APP_PORT
    app.listen(port, () => {
        Logger.info(`Listening on http://127.0.0.1:${port}`)
    })
}

startServer()
