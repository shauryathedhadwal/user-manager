import dotenv from 'dotenv'
import joi from 'joi'
import path from 'path'
import * as R from 'rambda'
import { ENV } from '../shared/constants'
import { APP_CONFIG_DEFAULTS, IAppConfig } from '../shared/interfaces'
import Logger from '../shared/logger'

dotenv.config({ path: path.join(__dirname, '../../environments/.env') })

let APP_CONFIG: IAppConfig

type AppConfigKeys = {
    [K in keyof IAppConfig] : joi.StringSchema | joi.NumberSchema | joi.BooleanSchema
}

const validateEnvSchema = () => {
    Logger.info('Validating Environment Variables Schema')

    const keys: AppConfigKeys = {
        // Add keys for validation here when updating .env
        NODE_ENV: joi.string().valid(ENV.LOCAL, ENV.DEV, ENV.STAG, ENV.PROD).required(),
        APP_PORT: joi.number().positive().required().description('Provide port for this application'),
        DB_URI: joi
            .string()
            .description(
                'Provide database uri for this application. E.g. : "postgresql://<username>:<password>@<host>/<database>"',
            ),
        CREATE_USER_IF_NOT_FOUND: joi.boolean().required(),
        DB_HOST: joi.string().required(),
        DB_NAME: joi.string().required(),
        DB_USERNAME: joi.string().required(),
        DB_PASSWORD: joi.string().required(),
        DB_SCHEMA_PREFIX: joi.string().required(),
        BCRYPT_SALT_ROUNDS: joi.number().default(APP_CONFIG_DEFAULTS.BCRYPT_SALT_ROUNDS),
        DB_DIALECT: joi.string().default(APP_CONFIG_DEFAULTS.DB_DIALECT),
        SENTRY_DSN: joi.string().required(),
        NEW_RELIC_APP_NAME: joi.string().required(),
        NEW_RELIC_LICENSE_KEY: joi.string().required()
    }

    const envSchema = joi.object<IAppConfig>().keys(keys).unknown()

    const { value: envVars, error } = envSchema.prefs({ errors: { label: 'key' } }).validate(process.env)

    if (error) {
        throw new Error(`Config validation error: ${error.message}`)
    }

    APP_CONFIG = R.pick(Object.keys(keys).join(','), envVars)
    Logger.debug(APP_CONFIG)
}

export const getAppConfig = () => {
    if (!APP_CONFIG) {
        validateEnvSchema()
    }

    return APP_CONFIG
}
