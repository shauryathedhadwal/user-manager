import { getAppConfig } from './configs/app.config'

/**
 * New Relic agent configuration.
 *
 * See lib/config/default.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
    app_name: [getAppConfig().NEW_RELIC_APP_NAME],
    license_key: getAppConfig().NEW_RELIC_LICENSE_KEY,
}
