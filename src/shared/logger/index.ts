import winston from 'winston'
import 'winston-daily-rotate-file'
import { ENV } from '../constants'

enum LEVELS {
    ERROR = 'error',
    WARN = 'warn',
    INFO = 'info',
    HTTP = 'http',
    DEBUG = 'debug',
}

const levels = {
    [LEVELS.ERROR]: 0,
    [LEVELS.WARN]: 1,
    [LEVELS.INFO]: 2,
    [LEVELS.HTTP]: 3,
    [LEVELS.DEBUG]: 4,
}

/**
 * This method set the current severity based on NODE_ENV
 * @returns severity level
 */
const level = () => {
    const env = process.env.NODE_ENV || ENV.DEV // Cannot call APP_CONFIG, will cause circular dependency
    const isDevelopment = env !== ENV.PROD
    return isDevelopment ? LEVELS.DEBUG : LEVELS.WARN
}

const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'white',
}

winston.addColors(colors)

const format = winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
    winston.format.colorize({ all: true }),
    winston.format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
)

const transports = [
    new winston.transports.Console(),
]

const Logger = winston.createLogger({
    level: level(),
    levels,
    format,
    transports,
})

export default Logger
