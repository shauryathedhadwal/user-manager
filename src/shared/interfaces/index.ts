import { ENV } from '../constants'

export interface IAppConfig {
    NODE_ENV: ENV
    APP_PORT: number
    DB_URI: string
    DB_DIALECT: string
    DB_SCHEMA_PREFIX: string
    DB_USERNAME: string
    DB_PASSWORD: string
    DB_HOST: string
    DB_NAME: string
    CREATE_USER_IF_NOT_FOUND: boolean
    BCRYPT_SALT_ROUNDS: number
    SENTRY_DSN: string
    NEW_RELIC_APP_NAME: string
    NEW_RELIC_LICENSE_KEY:string
}

export const APP_CONFIG_DEFAULTS: Partial<IAppConfig> = {
    BCRYPT_SALT_ROUNDS: 10,
    DB_DIALECT: 'postgres'
}
