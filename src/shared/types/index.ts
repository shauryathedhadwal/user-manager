export type EncryptedString = string
export type PlainTextString = string
export type DatabaseGeneratedId = number
