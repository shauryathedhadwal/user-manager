export enum ENV {
    LOCAL = 'local',
    DEV = 'development',
    STAG = 'staging',
    PROD = 'production',
}

export enum DB_TABLES {
    USER = 'user',
}
