/**
 * A handler accepts only req and res as params \
 * (next is intentionally not allowed).
 * A handler always returns (explicit or implicit) or throws.
 * If the returned value isSendable (determined by the isSendable validator) \
 * the onResponse callback is called.
 * If not, then an implicit next() is assumed and automatically applied
 * If the handler throws then the onError callback is called
 * it is assumed that no further processing is required \
 * after the onError or onResponse callbacks.
 */

import { NextFunction, Request, Response } from 'express'

export type Handler = (req: Request, res: Response, next: NextFunction) => Promise<any> | any
