import express from 'express'
import * as Sentry from '@sentry/node'
import * as Tracing from '@sentry/tracing'
import { CaptureConsole } from '@sentry/integrations'
import { getAppConfig } from '../configs/app.config'

let sentry: typeof Sentry

export const initSentry = (app: express.Application) => {

    Sentry.init({
        dsn: getAppConfig().SENTRY_DSN,
        integrations: [
            // enable HTTP calls tracing
            new Sentry.Integrations.Http({ tracing: true }),
            // enable Express.js middleware tracing
            new Tracing.Integrations.Express({ app }),
            new CaptureConsole({
                levels: ['error', 'info', 'log', 'warn'],
              }),
        ],
        
        // Set tracesSampleRate to 1.0 to capture 100%
        // of transactions for performance monitoring.
        // We recommend adjusting this value in production
        tracesSampleRate: 1.0,
    })

    sentry = Sentry /**Accessible by initSentryErrorHandlers*/
    
    // RequestHandler creates a separate execution context using domains, so that every
    // transaction/span/breadcrumb is attached to its own Hub instance
    app.use(sentry.Handlers.requestHandler())
    // TracingHandler creates a trace for every incoming request
    app.use(sentry.Handlers.tracingHandler())

}

export const initSentryErrorHandlers = (app: express.Application) => {
    // The error handler must be before any other error middleware and after all controllers
    app.use(sentry.Handlers.errorHandler())
}

