import { Request, Response, Application, NextFunction } from 'express'
import { ApplicationError } from './base-errors'
import Logger from '../logger'

type SentryIncludedResponse = Response & Record<string, any>

export const initErrorHandlerController = (app: Application) => {
    app.use((err: Error, req: Request, res: SentryIncludedResponse, next: NextFunction) => {
        if (err instanceof ApplicationError) {
            res.status(err.statusCode).json({ success: false, message: err.message, sentry: res.sentry})
            Logger.error(`type: ${err.type}`)
            Logger.error(err.data)
        } else {
            Logger.error(err)
            res.status(500)
                .json({ success: false, message: 'System Error' , sentry: res.sentry})
        }
    })
}

export * from './base-errors'
