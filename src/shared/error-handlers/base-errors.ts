class ApplicationError extends Error {
    constructor(private _message: string, private _data?: Record<string, any>) {
        super(_message) // Error class only takes message
        this._data = _data || undefined
    }

    get statusCode() {
        return 500
    }

    get data() {
        return this._data
    }

    get type(){
        return 'APPLICATION_ERROR'
    }
}

class DatabaseError extends ApplicationError {
    get type(){
        return 'DATABASE'
    }
}

class ClientSideError extends ApplicationError {
    get type(){
        return 'CLIENT'
    }
}

class RPCError extends ApplicationError {
    get type(){
        return 'RPC'
    }
}

export { ApplicationError, DatabaseError, ClientSideError, RPCError }
