import { getAppConfig } from '../../../configs/app.config'
import { Handler } from '../../../shared/api-utils/handler'
import Logger from '../../../shared/logger'
import db from '../../../db'
import { validatePassword } from '../user.service'
import { createUserObject } from '../user'
import { ApplicationError } from '../../../shared/error-handlers'

const controller: Handler = async (req, res, next) => {
    const { mobile } = req.params
    const { password } = req.body

    //TODO: Add error class
    if (!mobile || !password) {
        Logger.debug(`[VALIDATE] missing params ${mobile}`)
        return res.status(400).json({ success: false, message: 'Mobile Number or password missing' })
    }

    const config = getAppConfig()
    try {
        const user = await db.User.findOne({
            where: {
                mobile,
            },
        })

        // TODO: Temporary condition while User Database in not migrated
        if (config.CREATE_USER_IF_NOT_FOUND && !user) {
            const user = await createUserObject({ mobile, password })
            await db.User.create(user)
            Logger.debug(`[VALIDATE] created new user ${mobile}`)
            return res.json({ success: true })
        }

        if (!user || !user.password) {
            Logger.debug(`[VALIDATE] user not found`)
            return res.status(404).json({ success: false, message: 'User not found!' })
        }

        const isPasswordValid = await validatePassword(password, user.password)

        Logger.debug(`[VALIDATE] ${mobile} isPasswordValid: ${isPasswordValid}`)

        if (!isPasswordValid) {
            return res.status(400).json({ success: false, message: 'Invalid Credentials' })
        }

        return res.json({ success: true })
    } catch (error) {
        return next(new ApplicationError('There was an error while querying users', error))
    }
}

export default controller
