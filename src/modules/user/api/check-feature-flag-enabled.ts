import * as R from 'rambda'
import db from '../../../db'
import { Handler } from '../../../shared/api-utils/handler'
import { ApplicationError, DatabaseError } from '../../../shared/error-handlers'
import Logger from '../../../shared/logger'

const controller: Handler = async (req, res, next) => {
    try {
        const { mobile } = req.params

        if (!mobile) {
            return res.status(400).json({ success: false, message: 'Mobile Number is missing' })
        }
        const user = await db.User.findOne({ where: { mobile } })
        Logger.debug(`[FEATURE FLAG] ${mobile} exists: ${!R.isNil(user) || !R.isEmpty(user)}`)
        
        return res.json({
            data: {
                isFeatureFlagEnabled: R.path('featureFlag', user) === true,
                mobile: R.path('mobile', user),
            },
        })
    } catch (error) {
        return next(new ApplicationError('System Error', error))
    }
}

export default controller
