import db from '../../../db'
import { Handler } from '../../../shared/api-utils/handler'
import { DatabaseError } from '../../../shared/error-handlers'
import Logger from '../../../shared/logger'
import { createUserObject } from '../user'

const controller: Handler = async (req, res, next) => {
    try {
        const user = await createUserObject(req.body)
        await db.User.create(user)
        return res.json(user)
    } catch (error) {
        return next(new DatabaseError('There was an error creating user', error))
    }
}

export default controller
