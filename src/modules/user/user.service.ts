import bcrypt from 'bcrypt'
import { getAppConfig } from '../../configs/app.config'
import { EncryptedString, PlainTextString } from '../../shared/types'

export const hashPassword = async (stringToHash: PlainTextString): Promise<EncryptedString> => {
    const saltRounds = getAppConfig().BCRYPT_SALT_ROUNDS
    const salt = await bcrypt.genSalt(saltRounds)
    return bcrypt.hash(stringToHash, salt)
}

export const validatePassword = (passwordToValidate: PlainTextString, passwordInDB: EncryptedString) =>
    bcrypt.compare(passwordToValidate, passwordInDB)
