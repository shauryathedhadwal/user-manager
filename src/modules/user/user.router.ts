import express from 'express'
const router = express.Router()
import { default as validateCredentials } from './api/validate-credentials'
import { default as createNewUser } from './api/create-new-user'
import { default as checkFeatureFlagEnabled } from './api/check-feature-flag-enabled'

// TODO: SEGREGATE PUBLIC and PROTECTED ROUTES
// router.post('/', createNewUser)

// TODO: Handle both emailId and mobile
router.post('/:mobile/validate', validateCredentials)

router.get('/:mobile/feature-flag', checkFeatureFlagEnabled)

export default router
