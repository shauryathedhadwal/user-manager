import { IUserAttributes } from '../../db/user.model'
import { PlainTextString } from '../../shared/types'
import { hashPassword } from './user.service'

export type IUser = IUserAttributes

export interface ICreateUserRequestPayload {
    firstName?: string
    lastName?: string
    email?: string
    mobile: string
    password: PlainTextString
}

export const createUserObject = async (payload: ICreateUserRequestPayload): Promise<IUser> => {
    return {
        ...(payload.firstName && { firstName: payload.firstName }),
        ...(payload.lastName && { lastName: payload.lastName }),
        ...(payload.email && { emailId: payload.email }),
        mobile: payload.mobile,
        password: await hashPassword(payload.password),
    }
}
